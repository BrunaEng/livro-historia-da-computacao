# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores]()
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Primeira Geração]()
1. [Computação Móvel]()
1. [Futuro]()




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/8689107/avatar.png?width=400)  | Bruna Castro Fernandes | BrunaEng | [brunacastrofernandes@alunos.utfpr.edu.br](mailto:brunacastrofernandes@alunos.utfpr.edu.br)
